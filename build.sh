#

dot _includes/sites.dot -Tpng -o img/sites.png
jekyll build 1> /dev/null 2>&1

# update ipfs addresses
qmgen=$(ipfs add -r -Q _site/sites/127.0.0.1)
qmdum=$(ipfs add -r -Q _site/sites/dummy)
sed -i -e "s/qmgen: .*/qmgen: $qmgen/" -e "s/qmdummy: .*/qmdummy: $qmdum/" _data/ipfs.yml
jekyll build

www=$(echo "$(pwd)/_site" | sed -e 's,/home/,,');
echo url: http://localhost:8088/$www/index.html
xdg-open http://localhost:8088/$www/index.html

