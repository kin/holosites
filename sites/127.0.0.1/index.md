---
layout: default
title: genesis
description: "genesis block for holoSites"
---
This is the %name% page for all holoSites
<br>contact: [michelc@gc-bank.org](mailto:michelc@gc-bank.org)
{% assign qmurl="http://127.0.0.1:8080/ipfs/%qm%" %}
qm: [%qm%]({{qmurl}})

```
name: %name%
description: %description%
url: {{qmurl}}
```


