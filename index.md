---
layout: default
title: "%name%"
description: "%description%"
cssversion: 621c752
git_url: "https://github.com/Conscious-Evolution/holoSites.git"
---
# {{site.title}} : {{page.title}}

This website %domain% is hosted on the [KIN] network (on [IPFS][1]) and deployed with [netlify][2]
This web site repository is <{{page.git_url}}>

The IPFS address is <https://gateway.ipfs.io/ipfs/%qm%>

```yaml
name: {{page.title}}
domain: %domain%
description: {{page.description}}
qm: %qm%

```

[![sites](img/sites.png)](sites-dot.html)

you will find config.json [files][3] for the following sites :

* <https://kind™.ml>
* <https://www.holofund.ml>
* <https://www.chii.ml>


--&nbsp;<br>
+holoTeam

[KIN]: https://holoKIN.ml
[1]: {{site.search}}=IPFS
[2]: https://app.netlify.com/sites/{{site.name}}/deploys
[3]: sites

